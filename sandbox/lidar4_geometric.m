% load lidar data
veloReader = velodyneFileReader('./recordings/2021-12-15-11-21-38_Velodyne-VLP-16-Data.pcap','VLP16');
veloReader.CurrentTime = veloReader.StartTime + seconds(9.1);

ptCloud = readFrame(veloReader);

% ground segmentation
maxDistance = 0.3;
referenceVector = [0 0 1];
[~, inliers, outliers] = pcfitplane(ptCloud, maxDistance, referenceVector);
ptCloudWithoutGround = select(ptCloud, outliers, 'OutputSize', 'full');

hSegment = figure;
panel = uipanel('Parent', hSegment, 'BackgroundColor',[0 0 0 ]);
ax = axes('Parent', panel, 'Color', [0 0 0 ]);
pcshowpair(ptCloud, ptCloudWithoutGround, 'Parent', ax);
legend('Ground Region', 'Non-Ground Region', 'TextColor', [1 1 1]);
title('Segmented Ground Plane');

% bounding boxes

distThreshold = 1;
[labels, numClusters] = pcsegdist(ptCloudWithoutGround, distThreshold);
labelColorIndex = labels;

hCuboid = figure;
panel = uipanel('Parent', hCuboid, 'BackgroundColor', [0 0 0 ]);
ax = axes('Parent', panel, 'Color', [0 0 0 ]);
pcshow(ptCloud.Location, labelColorIndex, 'Parent', ax);
title('Fitting Bounding Boxes');
hold on;

for i = 1:numClusters
    idx = find(labels == i);
    model = pcfitcuboid(ptCloudWithoutGround, idx);
    plot(model);
end
