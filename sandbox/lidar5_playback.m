% config
saveGif = false;

% load lidar data
veloReader = velodyneFileReader('./recordings/2021-12-15-11-21-38_Velodyne-VLP-16-Data.pcap','VLP16');
veloReader.CurrentTime = veloReader.StartTime + seconds(20);

% create lidar player
xlimits = [-50, 50];
ylimits = [-10, 10];
zlimits = [-5, 5];
%player = pcplayer(xlimits, ylimits, zlimits);

% configure the plot
% xlabel(player.Axes, 'X (m)');
% ylabel(player.Axes, 'Y (m)');
% zlabel(player.Axes, 'Z (m)');

colors = [0 1 0; 1 0 0];
greenIdx = 1;
redIdx = 2;

hView = figure;
playbackPanel = uipanel('Parent', hView, 'BackgroundColor', [0 0 0 ]);
playbackAxes = axes('Parent', playbackPanel, 'Color', [0 0 0], 'XLim', [-30 30]);
xlabel(playbackAxes, 'X (m)');
ylabel(playbackAxes, 'Y (m)');
zlabel(playbackAxes, 'Z (m)');
%xlim(playbackAxes, 'manual');% [-30, 30]);
%ylim(playbackAxes, 'manual');% [-30, 30]);

%playbackAxes.XLim = [-30 30];
%playbackAxes.XLimMode = 'manual';
%playbackAxes.YLim = [-30 30];
%set(playbackAxes, 'XLim', [-30 30]);
%playbackAxes.YLimMode = 'manual';
%playbackAxes.ZLim = [-10 10];
%playbackAxes.ZLimMode = 'manual';
    hold(playbackAxes, 'on');

assignmentGate = [75 1000]; % Assignment threshold;
confThreshold = [7 10];    % Confirmation threshold for history logic
delThreshold = [8 10];     % Deletion threshold for history logic
Kc = 1e-9;

% create the tracker
tracker = trackerJPDA('TrackLogic', 'History', ...
    'AssignmentThreshold', assignmentGate, ...
    'ConfirmationThreshold', confThreshold, ...
    'DeletionThreshold', delThreshold);


%tracker = trackerJPDA('FilterInitializationFcn',filterInitFcn,...
%    'TrackLogic','History',...
%    'AssignmentThreshold',assignmentGate,...
%    'ClutterDensity',Kc,...
%    'ConfirmationThreshold',confThreshold,...
%    'DeletionThreshold',delThreshold,...
%    'HasDetectableTrackIDsInput',true,...
%    'InitializationThreshold',0,...
%    'HitMissThreshold',0.1);

roi = [ -60 60 -5 5 -3 3]; % x, y, z range

k = 0;
while (hasFrame(veloReader) && veloReader.CurrentTime < veloReader.StartTime + seconds(30))

    time = seconds(veloReader.CurrentTime - veloReader.StartTime);
    disp('Time');
    disp(time);

    % campos(playbackAxes, [-20, -30, 10]);
    k = k + 1;
%    set(playbackAxes, 'XLim', [-30 30]);

%xlim(playbackAxes, 'manual');% [-30, 30]);
%ylim(playbackAxes, 'manual');% [-30, 30]);

    cla(playbackAxes);
%xlim(playbackAxes, 'manual');% [-30, 30]);
%ylim(playbackAxes, 'manual');% [-30, 30]);

%xlim(playbackAxes, [-30, 30]);
%ylim(playbackAxes, [-30, 30]);

    % read the current frame
    ptCloud = veloReader.readFrame();

    % filter points in ROI (range of interest)
%    disp(size(ptCloud.Location));

    % create label array for coloring
    %colorLabels = zeros(size(ptCloud.Location, 1), size(ptCloud.Location, 2));

    % segment out ground from the point cloud
    %[groundIndices, ptCloudWithoutGround] = groundSegmentation(ptCloud);

    %[row, col] = ind2sub(size(ptCloud.Location), groundIndices);
    %disp(row);
    %disp(col);
    %disp(cat(2, col, row));
    %indices2 = cat(2, row, col);
%    break;

    %view(player, ptCloudWithoutGround.Location);

    groundIndices = segmentGroundFromLidarData(ptCloud);
    ptCloudWithoutGround = select(ptCloud, ~groundIndices, "OutputSize", "full");

    %colorLabels(groundIndices(:)) = greenIdx;
    %colorLabels(~groundIndices(:)) = redIdx;

    inRoiIndices = findPointsInROI(ptCloudWithoutGround,roi);
%    disp(size(ptCloud.Location));
    roiPtCloud = select(ptCloudWithoutGround, inRoiIndices, 'OutputSize', 'full');
    %roiPtCloud = ptCloudWithoutGround;

    distThreshold = 1.8;
    angleThreshold = 30;
    minDotsPerCluster = 50;
    minZDistance = -3;
    maxZDistance = 3;
    numAllowedClusterPoints = [minDotsPerCluster; inf];
    [labels, numClusters] = segmentLidarData(roiPtCloud, distThreshold, angleThreshold, NumClusterPoints=numAllowedClusterPoints);

    % set the colormap for the player, the last index will be the ground
    map = [hsv(numClusters);1 0 1];
    colormap(playbackAxes, map);
    
    % add a cluster for the ground
    labels(groundIndices) = numClusters + 1;
    
    pcshow(ptCloud.Location, labels, 'Parent', playbackAxes);

    % fit cuboids for the cluster without the ground
    %cla(player.Axes);

    bboxes = nan(numClusters, 3);
    isValidCluster = false(numClusters, 1);
    plot_models = cell(0);

    for i = 1:numClusters
        indices = find(labels == i);

        disp(size(roiPtCloud.Location));
        meanPoint = mean(roiPtCloud.Location, [1,2], 'omitnan');
        disp(size(meanPoint));
        %exit;
        if size(indices, 1) > minDotsPerCluster && meanPoint(3) < maxZDistance && meanPoint(3) > minZDistance
            model = pcfitcuboid(roiPtCloud, indices);

            % use required parameters for the tracking
            yaw = model.Orientation(3);
            L = model.Dimensions(1);
            W = model.Dimensions(2);
            H = model.Dimensions(3);

            bboxes(i,:) = [model.Center];
            isValidCluster(i) = L < 10 && W < 10;

            %disp(model);
            if isValidCluster(i)
                %plot_models(end+1) = model;
                plot(model, 'Parent', playbackAxes);
            end
        end
    end

%    disp(size(plot_models));    
%    plot(plot_models, 'Parent', playbackAxes);


    % use only valid clusters
    bboxes = bboxes(isValidCluster, :);

    % create object detections from boxes
    numBoxes = size(bboxes, 1);
    detections = cell(numBoxes, 1);
    for i = 1:numBoxes
        detections{i} = objectDetection(time, cast(bboxes(i,:), 'double'));
    end
   % break;

%    numBoxes = size(bboxes,2);
%detections = cell(numBoxes,1);
%for i = 1:numBoxes
%    detections{i} = objectDetection(time,cast(bboxes(:,i),'double'),...
%        'MeasurementNoise',double(measNoise),'ObjectAttributes',struct,...
%        'MeasurementParameters',measParams);
%end

%    disp(bboxes);
   % disp(detections);

    % ok, we have the detections, we feed them into the tracker
    disp('detections');
    disp(size(detections));
    if (k > 1)
        %detections = detections(1:6, :);
    end
    [ confirmedTracks, tentativeTracks, allTracks, info ] = tracker(detections, time);
    
    disp('confirmed tracks');
    disp(size(confirmedTracks));

    %tp = theaterPlot('Parent', playbackAxes);
    %trkPlotter = platformPlotter(tp, 'DisplayName','Tracks');

    %trackPlotter.plot
    %plotTrack(trackPlotter, )
    %trkPlotter.plotPlatform([0 0 0], struct('Length',1,'Width',1,'Height',1,'OriginOffset',[0 0 0]),eye(3));

   % trkPlotter.plotTrack(   )

    %disp(bboxes)
%    str = struct('Length', 0, 'Width', 0, 'Height', 0, 'OriginOffset', [0, 0, 0 ], 'Orientation', [0, 0, 0]);
   %drawnow;

    if (size(confirmedTracks,1) > 0)

        for i = 1:size(confirmedTracks, 1)
            trackState = confirmedTracks(i).State;

            velocity = [trackState(2), trackState(4), trackState(6)];
            if (norm(velocity) > 1)
                info = [confirmedTracks(i).TrackID, velocity];
              disp(info);
                 tmp_box = cuboidModel([trackState(1), trackState(3), trackState(5), 4, 4, 4, 0, 0, 0]);
                plot(tmp_box, 'Parent', playbackAxes);
            end
            %disp(tmp_box);
            %break;


        end

        %allTrackState = cat(1, confirmedTracks.State);

        %disp(size(allTrackState));
        %disp(confirmedTracks(1));
        %break;
    end

    % Pass detections to track.
    %[confirmedTracks,tentativeTracks,allTracks,info] = tracker(detections,time,detectableTracksInput);
    %numTracks(i,1) = numel(confirmedTracks);

    xlim(playbackAxes, [-50, 50]);
    ylim(playbackAxes, [-30, 30]);
    zlim(playbackAxes, [-5, 5]);
    campos(playbackAxes, [-298.1808610874417,-0.487849638207576,126.4162732662746]);
    camproj(playbackAxes, 'perspective');

    %disp(campos(playbackAxes));
%   
    %hold(playbackAxes, 'off');
    drawnow;
    %break;
%https://www.mathworks.com/help/fusion/ug/track-vehicles-using-lidar.html

    if saveGif
        frame = getframe(playbackAxes);
        image = frame2im(frame);
        [imind, cm] = rgb2ind(image, 256); % Write to GIF
        filename = 'output.gif';
        if k == 1
            imwrite(imind, cm, filename, 'gif', 'Loopcount', inf);
        else
            imwrite(imind, cm, filename, 'gif', 'WriteMode', 'append');
        end
    end
    %imwrite(imind, cm, 'outputtest', 'gif', 'Loopcount', inf);
    %imwrite(image, 'alma' + k, 'png');

    if k == 10
    %    break;
    end
    %imwrite(frame.cdata, 'alma.png')
    %break;
    % 
    %labelColorIndex = labels - 10;

    %colormap([hsv(numClusters);[1 1 0]]);


    %disp(size(labels));
    %disp(labels);
    %disp(numClusters);
    %disp(labelColorIndex);
    %disp(numClusters);

    %figure;
    %pcshow(ptCloud.Location, labelColorIndex);
    %view(player, ptCloud.Location, labels);
    %break;
   % break;
    %colormap([hsv(numClusters);[0 1 0]]);
    %break;
  %  break;

%     disp('ground')
%     disp(size(groundPtsIdx));
% 
%     disp(size(groundIndices(:)));
%     disp(size(groundIndices));
%     disp(size(colorLabels));
%     disp(groundIndices);
% 
%     B = reshape(groundIndices, size(ptCloud.Location));
%     disp(B);

    %disp(groundPtsIdx);
   % break;
   pause(0.05);
end

function [groundIndices, ptCloudWithoutGround] = groundSegmentation(ptCloud)

    maxDistance = 0.5; % maximum distance from the plane
    referenceVector = [ 0 0 1]; % reference normal vector of the ground plane

    % fit the plane and return the inliers and outliers
    [~, groundIndices, outlierIndices] = pcfitplane(ptCloud, maxDistance, referenceVector);

    % select points without the ground
    ptCloudWithoutGround = select(ptCloud, outlierIndices, 'OutputSize', 'full');
end

% https://www.mathworks.com/help/fusion/ug/track-vehicles-using-lidar.html
% https://www.mathworks.com/help/vision/ref/segmentlidardata.html
% https://www.mathworks.com/help/vision/ref/segmentgroundfromlidardata.html
% https://www.matlabexpo.com/content/dam/mathworks/mathworks-dot-com/images/events/matlabexpo/in/2019/lidar-processing-for-automated-driving.pdf
% https://www.mathworks.com/help/lidar/ref/pcfitcuboid.html
