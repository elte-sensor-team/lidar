% Set random seed to generate reproducible results.
S = rng(2018);

% A bounding box detector model.
detectorModel = HelperBoundingBoxDetector(...
    'XLimits',[-60 60],...              % min-max
    'YLimits',[-8 5],...                % min-max
    'ZLimits',[-2 5],...                % min-max
    'SegmentationMinDistance',0.5,...   % minimum Euclidian distance
    'MinDetectionsPerCluster',50,...     % minimum points per cluster
    'MeasurementNoise',blkdiag(0.25*eye(3),25,eye(3)),...       % measurement noise in detection report
    'GroundMaxDistance',0.3);           % maximum distance of ground points from ground plane

assignmentGate = [75 1000]; % Assignment threshold;
confThreshold = [7 10];    % Confirmation threshold for history logic
delThreshold = [8 10];     % Deletion threshold for history logic
Kc = 1e-9;                 % False-alarm rate per unit volume

% IMM filter initialization function
filterInitFcn = @helperInitIMMFilter;

% A joint probabilistic data association tracker with IMM filter
tracker = trackerJPDA('FilterInitializationFcn',filterInitFcn,...
    'TrackLogic','History',...
    'AssignmentThreshold',assignmentGate,...
    'ClutterDensity',Kc,...
    'ConfirmationThreshold',confThreshold,...
    'DeletionThreshold',delThreshold,...
    'HasDetectableTrackIDsInput',true,...
    'InitializationThreshold',0,...
    'HitMissThreshold',0.1);

% Create display
displayObject = HelperLidarExampleDisplay([],...
    'PositionIndex',[1 3 6],...
    'VelocityIndex',[2 4 7],...
    'DimensionIndex',[9 10 11],...
    'YawIndex',8,...
    'MovieName','output',...  % Specify a movie name to record a movie.
    'RecordGIF',true); % Specify true to record new GIFs

%% Loop Through Data
% Loop through the recorded lidar data, generate detections from the
% current point cloud using the detector model and then process the
% detections using the tracker.

% Initiate all tracks.
allTracks = struct([]);

startTime = seconds(20);
veloReader = velodyneFileReader('./recordings/2021-12-15-11-21-38_Velodyne-VLP-16-Data.pcap','VLP16');
veloReader.CurrentTime = veloReader.StartTime + startTime;

vehicleCounter = [];

% Loop through the data
while (hasFrame(veloReader))

    time = seconds(veloReader.CurrentTime - veloReader.StartTime);

    ptCloud = veloReader.readFrame();

    % Make it unorganized
    ptCloudUnorg = removeInvalidPoints(ptCloud);

    % Rotate horizontally
    theta = 0.349;
    rot = [cos(theta) sin(theta) 0; ...
         -sin(theta) cos(theta) 0; ...
               0          0  1];
    trans = [0, 0, 0];
    tform = rigid3d(rot,trans);

    ptCloudUnorg = pctransform(ptCloudUnorg, tform);
    currentLidar = ptCloudUnorg;
    
    % Generator detections from lidar scan.
    [detections,obstacleIndices,groundIndices,croppedIndices] = detectorModel(currentLidar,time);
    
    % Calculate detectability of each track.
    detectableTracksInput = helperCalcDetectability(allTracks,[1 3 6]);
    
    % Pass detections to track.
    [confirmedTracks,tentativeTracks,allTracks,info] = tracker(detections,time,detectableTracksInput);

    % Count confirmed tracks
    for ii = 1:numel(confirmedTracks)
        if ~any(vehicleCounter == confirmedTracks(ii).TrackID)
            vehicleCounter = [ confirmedTracks(ii).TrackID vehicleCounter ];
        end
    end

    % Get model probabilities from IMM filter of each track using
    % getTrackFilterProperties function of the tracker.
    modelProbs = zeros(2,numel(confirmedTracks));
    for k = 1:numel(confirmedTracks)
        c1 = getTrackFilterProperties(tracker,confirmedTracks(k).TrackID,'ModelProbabilities');
        modelProbs(:,k) = c1{1};
    end
    
    % Update display
    if isvalid(displayObject.PointCloudProcessingDisplay.ObstaclePlotter)
        
        % Update display object
        displayObject(detections,confirmedTracks,currentLidar,obstacleIndices,...
            groundIndices,croppedIndices,'', size(vehicleCounter, 2), modelProbs);
    end
end

% Write movie if requested
if ~isempty(displayObject.MovieName)
    writeMovie(displayObject);
end

% Write new GIFs if requested.
if displayObject.RecordGIF
    % second input is start frame, third input is end frame and last input
    % is a character vector specifying the panel to record.
    writeAnimatedGIF(displayObject,10,170,'trackMaintenance','ego');
    writeAnimatedGIF(displayObject,310,330,'jpda','processing');
    writeAnimatedGIF(displayObject,120,140,'imm','details');
end



